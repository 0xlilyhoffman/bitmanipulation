#include <stdio.h>

//BIT MANIPULATIONS
int bitXor(int x, int y);
int evenBits();
int oddBits();
int byteSwap(int x, int n, int m); 
int logicalShift(int x, int n);

//TWOS COMPLEMENT ARITHMETIC
int isZero(int x);
int tmax(void);
int fitsBits(int x, int n);
int sign(int x); 
int isGreater(int x, int y);
int absVal(int x); 
int negate(int x); 
int minusOne(void);




int main(int argc, char *argv[]) {
	int z =  isGreater(5, 4);
	printf("%d", z);
}

/**********************--------------------------***********************/
/**********************     BIT MANIPULATIONS    ***********************/
/**********************--------------------------***********************/

/* 
 * bitXor - x^y using only ~ and & 
 *   Example: bitXor(4, 5) = 1
 *   Legal ops: ~ &
 *   Max ops: 14
 *   Rating: 1
 */
int bitXor(int x, int y){
	int bits_both_ones = x&y;
	int bits_both_zeros = ~x & ~y;
	int bit_x_or = ~(bits_both_ones) & ~(bits_both_zeros);
	return bit_x_or;
}
/* 
 * evenBits - return word with all even-numbered bits set to 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 8
 *   Rating: 1
 */
int even_bits(){
	int byte_1 = 0b01010101;
	int byte_2 =byte_1 << 8;
	int bottom_two_bytes = byte_1 | byte_2;
	
	int top_two_bytes = bottom_two_bytes <<16;
	int final = bottom_two_bytes | top_two_bytes;
	return final;
}
/* 
 * byteSwap - swaps the nth byte and the mth byte
 *  Examples: byteSwap(0x12345678, 1, 3) = 0x56341278
 *            byteSwap(0xDEADBEEF, 0, 2) = 0xDEEFBEAD
 *  You may assume that 0 <= n <= 3, 0 <= m <= 3
 *  Legal ops: ! ~ & ^ | + << >>
 *  Max ops: 25
 *  Rating: 2
 */
int byteSwap(int x, int n, int m){
	int mask = 255; //255 is 3 bytes of 0 and 1 byte of all 1
	int byte_n = (x>> (n<<3))&mask; 
	/*shift (8*n bits = n bytes) ==> byte_n now bottom byte of int preceded by 3 bytes of 0*/
	int byte_m = (x>>((m<<3)))&mask;
	//bytes extracted; now swap them
	
	//move byte_n to location m 
	byte_n = byte_n << (m<<3);
	
	//move byte_n to location n
	byte_m = byte_m << (n<<3);
	int combine_swapped_bytes = byte_n | byte_m;  //11 ops
	
	//put zeros where byte m and n were and ones otherwise
	int mask_byte_n = ~(255<< ((n<<3)));
	int mask_byte_m = ~(255<< ((m<<3)));
	int combine_masks = mask_byte_n & mask_byte_m;
	
	int combine_masks_with_x = combine_masks & x;
	
	int ans = combine_swapped_bytes | combine_masks_with_x;
	return ans;
}
/* 
 * oddBits - return word with all odd-numbered bits set to 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 8
 *   Rating: 2
 */
int oddBits(){
	int byte_1 = 0b10101010;
	int byte_2 =byte_1 << 8;
	int bottom_two_bytes = byte_1 | byte_2;
	
	int top_two_bytes = bottom_two_bytes <<16;
	int final = bottom_two_bytes | top_two_bytes;
	return final;
}
/* 
 * logicalShift - shift x to the right by n, using a logical shift
 *   Can assume that 0 <= n <= 31
 *   Examples: logicalShift(0x87654321,4) = 0x08765432
 *   Legal ops: ~ & ^ | + << >>
 *   Max ops: 20
 *   Rating: 3 
 */
int logicalShift(int x, int n){
	int n_ones = (1<<n)+ ((~1) + 1); //(2^n)-1                   
	int n_ones_first = n_ones << (32 + ((~n) + 1)); //shift top bits 32-n   
	int n_zeros_first = ~(n_ones_first);                                   
	int arithmetic_shift = x >> n;		                                   
	int logical_shift = n_zeros_first & arithmetic_shift;                 
	return logical_shift; 						                           
}

/**************-----------------------------------------****************/
/**************       TWOS COMPLEMENT ARITHMETIC        ****************/
/**************-----------------------------------------****************/
/*
 * isZero - returns 1 if x == 0, and 0 otherwise 
 *   Examples: isZero(5) = 0, isZero(0) = 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 2
 *   Rating: 1
 */
int isZero(int x){
	return !x;
}
/* 
 * TMax - return maximum two's complement integer 
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 4
 *   Rating: 1
 */
int tmax(){
	int one_followed_by_all_zeros = 1 << 31;
	int zero_followed_by_all_ones =  ~(one_followed_by_all_zeros);
	return zero_followed_by_all_ones;
}
/* 
 * minusOne - return a value of -1 
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 2
 *   Rating: 1
 */
int minusOne(void){
	return (~1) + 1; 
}
/* 
 * fitsBits - return 1 if x can be represented as an 
 *  n-bit, two's complement integer.
 *   1 <= n <= 32
 *   Examples: fitsBits(5,3) = 0, fitsBits(-4,3) = 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 15
 *   Rating: 2
 */
int fitsBits(int x, int n){
	int ones_offset = 32 + ((~n) + 1); //32-n 
	int fill_ones = ((x << ones_offset)>>ones_offset);
	int ans = !(fill_ones ^ x); // !(fill_ones == x)
	return ans;
}
/* 
 * negate - return -x 
 *   Example: negate(1) = -1.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 5
 *   Rating: 2
 */
int negate(int x){
	return (~x) + 1; //invert all bits, add 1
}

/* 
 * sign - return 1 if positive, 0 if zero, and -1 if negative
 *  Examples: sign(130) = 1
 *            sign(-23) = -1
 *  Legal ops: ! ~ & ^ | + << >>
 *  Max ops: 10
 *  Rating: 2
 */
int sign(int x) {
	int first = x>>31;
	first = first<<1;
	int second = !(!x);
	
	int sign = first | second;
	
	return sign;
}
/* 
 * isGreater - if x > y  then return 1, else return 0 
 *   Example: isGreater(4,5) = 0, isGreater(5,4) = 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 24
 *   Rating: 3
 */
int isGreater(int x, int y){
	int difference = y + ((~x) + 1); //y-x neg if x>y
	int sign_bit = difference >> 31;
	int mask = 1;
	
	int answer = sign_bit & mask;
	return answer;
}
/* 
 * absVal - absolute value of x
 *   Example: absVal(-1) = 1.
 *   You may assume -TMax <= x <= TMax
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 10
 *   Rating: 4
 */
int absVal(int x) {
	int sign_bit = x >>31;
	int cascade = sign_bit + x;
	int abs_value = cascade^sign_bit;
	return abs_value;
	
}
