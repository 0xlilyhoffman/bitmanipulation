# Bit Manipulations

This project consisted of a series of puzzles to solve using only straightening code (i.e. no loops or conditionals) and a limited number of C arithmetic and logical operators 

```

int bitXor(int x, int y){
	Description: Perform x^y
	Legal Operators: ~ &
	Max Operators: 14
}

int evenBits(){
	Description: Return word with all even-numbered bits set to 1
	Legal Operators: ! ~ & ^ | + << >>
	Max Operators: 8
}

int oddBits(){
	Description: Return word with all odd-numbered bits set to 1
	Legal Operators:! ~ & ^ | + << >>
	Max Operators: 8
}

int byteSwap(int x, int n, int m){
	Description: Swap the nth byte and the mth byte of x
	Legal Operators: ! ~ & ^ | + << >>
	Max Operators: 25
}

int logicalShift(int x, int n){
	Description: Shift x to the right by n, using a logical shift
	Legal Operators: ~ & ^ | + << >>
	Max Operators: 20
}

int isZero(int x){
	Description: Return 1 if x == 0, and 0 otherwise
	Legal Operators: ! ~ & ^ | + << >>
	Max Operators: 2
}

int tmax(){
	Description: Return the maximum two's complement integer
	Legal Operators: ! ~ & ^ | + << >>
	Max Operators: 4
}

int minusOne(){
	Description: Perform x^y
	Legal Operators: ! ~ & ^ | + << >>
	Max Operators: 2
}

int fitsBits(int x, int n){
	Description: Return 1 if x can be represented as an n-bit, two's complement integer.
	Legal Operators: ! ~ & ^ | + << >>
	Max Operators: 15
}

int negate(int x){
	Description: Return -x
	Legal Operators: ! ~ & ^ | + << >>
	Max Operators: 15
}

int sign(int x) {
	Description: Return 1 if x is positive, 0 if x is zero, and -1 if x is negative
	Legal Operators: ! ~ & ^ | + << >>
	Max Operators: 10
}

int isGreater(int x, int y){
	Description: Return 1 if x can be represented as an n-bit, two's complement integer.
	Legal Operators: ! ~ & ^ | + << >>
	Max Operators: 24
}

int absVal(int x) {
	Description: Return 1 if x can be represented as an n-bit, two's complement integer.
	Legal Operators: ! ~ & ^ | + << >>
	Max Operators: 10
}

```
